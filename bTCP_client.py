#!/bin/python3
# s1005864 Lars Kuipers, s1011387 Steven Wallis de Vries

import argparse
import logging
import socket
import struct
import sys

import btcp

logging.basicConfig(stream=sys.stderr, level=logging.ERROR)

parser = argparse.ArgumentParser()
parser.add_argument("-w", "--window", help="Define bTCP window size", type=int, default=80)
parser.add_argument("-t", "--timeout", help="Define bTCP timeout in milliseconds", type=int, default=100)
parser.add_argument("-i", "--input", help="File to send", default="tmp.file")
args = parser.parse_args()

binding = btcp.Binding(socket.AF_INET, None, args.window, args.timeout)
connection = binding.connect_client(0, 0, ("localhost", 9001))

file = open(args.input, "r+b")
data = file.read()
file.close()

connection.send(struct.pack("!Q", len(data)))
connection.send(data)

binding.close()
