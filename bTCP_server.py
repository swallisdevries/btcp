#!/bin/python3
# s1005864 Lars Kuipers, s1011387 Steven Wallis de Vries

import argparse
import logging
import socket
import struct
import sys

import btcp

logging.basicConfig(stream=sys.stderr, level=logging.ERROR)

parser = argparse.ArgumentParser()
parser.add_argument("-w", "--window", help="Define bTCP window size", type=int, default=80)
parser.add_argument("-t", "--timeout", help="Define bTCP timeout in milliseconds", type=int, default=100)
parser.add_argument("-o", "--output", help="Where to store file", default="tmp.file")
args = parser.parse_args()

binding = btcp.Binding(socket.AF_INET, ("localhost", 9001), args.window, args.timeout)
server = binding.bind_server(0)

server.start_listen(1)
connection = server.accept()
connection.close_sender()

file_size: int = struct.unpack("!Q", connection.receive(8))[0]

file = open(args.output, "w+b")
data = connection.receive_array(file_size)
for block in data:
    file.write(block)
file.close()

binding.close()
