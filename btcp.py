# s1005864 Lars Kuipers, s1011387 Steven Wallis de Vries

import binascii
import bisect
import copy
import logging
import random
import socket
import struct
import threading
import time
from typing import *

header_format = "!HHHHBBHI"
header_size = 16
payload_size = 1000
max_seq = 0xffFF

enable_more_asserts = False
debug_window = False


class _Flags:
    def __init__(self, flags: Union[int, Tuple[bool, bool, bool]] = 0):
        """
        :param flags: Combined or (SYN, ACK, FIN)
        """
        if type(flags) is int:
            self.fin = bool(flags & 1)
            self.ack = bool(flags & 1 << 1)
            self.syn = bool(flags & 1 << 2)
        else:
            self.syn, self.ack, self.fin = flags

    def __int__(self) -> int:
        return self.syn << 2 | self.ack << 1 | self.fin

    def __str__(self) -> str:
        strs: List[str] = []
        if self.syn:
            strs.append("SYN")
        if self.ack:
            strs.append("ACK")
        if self.fin:
            strs.append("FIN")
        return "-".join(strs)


class _Header:
    @staticmethod
    def unpack(header: bytes) -> "_Header":
        return _Header(*struct.unpack(header_format, header))

    def __init__(self, src_port: int, dest_port: int, syn_nr: int, ack_nr: int, flags: int,
                 window: int, data_length: int, checksum: int = 0):
        self.dest_port = dest_port
        self.src_port = src_port
        self.syn_nr = syn_nr
        self.ack_nr = ack_nr
        self.flags = flags
        self.window = window
        self.data_length = data_length
        self.checksum = checksum

    def flags_obj(self) -> _Flags:
        return _Flags(self.flags)

    def __bytes__(self) -> bytes:
        return struct.pack(
            header_format,
            self.src_port,
            self.dest_port,
            self.syn_nr,
            self.ack_nr,
            self.flags,
            self.window,
            self.data_length,
            self.checksum)


class _Packet:
    def __init__(self, header: _Header, data: bytes):
        self.header = header
        self.data = data

    def compute_checksum(self) -> int:
        if self.header.checksum == 0:
            header_no_checksum = self.header
        else:
            header_no_checksum = copy.deepcopy(self.header)
            header_no_checksum.checksum = 0
        return binascii.crc32(bytes(header_no_checksum) + self.data)

    def verify_checksum(self) -> bool:
        return self.compute_checksum() == self.header.checksum

    def set_checksum(self) -> None:
        self.header.checksum = self.compute_checksum()

    def __lt__(self, other: "_Packet") -> bool:
        return self.header.syn_nr < other.header.syn_nr


class _TimestampPacket(_Packet):
    def __init__(self, header: _Header, data: bytes, timestamp: float):
        super().__init__(header, data)
        self.timestamp = timestamp


class _AddrPacket(_Packet):
    def __init__(self, header: _Header, data: bytes, remote_udp_addr: Tuple[str, int]):
        super().__init__(header, data)
        self.remote_udp_addr = remote_udp_addr


# Info for if the connection was not initiated from our side
class _RemoteInit:
    def __init__(self, syn_nr: int, window_size: int):
        self.syn_nr = syn_nr
        self.window_size = window_size


class _Stream:
    def __init__(self, binding: "Binding", local_port: int,
                 remote_port: int, remote_udp_addr: Any,
                 local_window_size: int, timeout: float,
                 remote_init: Optional[_RemoteInit] = None):
        self.binding = binding
        self.local_port = local_port
        self.remote_port = remote_port
        self.local_window_size = local_window_size
        self.remote_udp_addr = remote_udp_addr
        self.timeout = timeout
        self.expect_syn_ack = remote_init is None

        self.send_buffer: List[bytes] = []  # Data to be send
        self.sent_ack_buffer: List[_TimestampPacket] = []  # Data still to be ACKed by other

        self.receive_ack_buffer: List[_Packet] = []  # Received but not ACKed by me
        self.receive_buffer: List[bytes] = []  # Received and ACKed, but not yet retrieved

        self.send_buffer_lock = threading.Lock()
        self.send_buffer_empty = threading.Condition(self.send_buffer_lock)
        self.receive_buffer_lock = threading.Lock()
        self.receive_bytes_available = threading.Condition(self.receive_buffer_lock)

        self.all_acked_lock = threading.Lock()
        self.all_acked = threading.Condition(self.all_acked_lock)

        self.receiver_closed = False  # If the remote host has sent a FIN
        self.sender_closed = False  # If we have sent a FIN

        self.receiver_closed_lock = threading.Lock()
        self.receiver_closed_event = threading.Condition(self.receiver_closed_lock)

        self.deb = f"{self.binding.local_udp_addr}|{self.local_port}->{self.remote_port}: "

        self.first_send_syn_nr = random.randint(0, max_seq)

        if remote_init is None:  # We initiate the connection
            self.send_syn_nr = self.first_send_syn_nr  # Next to be sent
            self.send_ack_nr = self.send_syn_nr  # First not ACKed by other

            self.first_recv_syn_nr = None

            self.__send_to_be_acked(b"", self.send_syn_nr, _Flags((True, False, False)))

        else:
            self.remote_window_size = remote_init.window_size

            self.recv_syn_nr = remote_init.syn_nr  # Last received
            self.recv_ack_nr = self.__next_seq(remote_init.syn_nr)  # First still to be received
            self.send_syn_nr = self.first_send_syn_nr  # Next to be sent
            self.send_ack_nr = self.send_syn_nr  # First not ACKed by other

            self.first_recv_syn_nr = self.recv_syn_nr

            self.__send_to_be_acked(b"", self.send_syn_nr, _Flags((True, True, False)))

        self.send_syn_nr = self.__next_seq(self.send_syn_nr)
        logging.debug(self.deb + "Setup stream")

    @staticmethod
    def __next_seq(nr: int) -> int:
        return (nr + 1) % max_seq

    @staticmethod
    def __seq_between(nr: int, lower_bound: int, upper_bound: int) -> bool:
        nr %= max_seq
        lower_bound %= max_seq
        upper_bound %= max_seq
        if lower_bound <= upper_bound:
            return lower_bound <= nr <= upper_bound
        else:
            return nr >= lower_bound or nr <= upper_bound

    def __send_to_be_acked(self, data: bytes, syn_nr: int, flags=_Flags((False, True, False))) -> None:
        logging.debug(self.deb +
                      f"Sending #{syn_nr} {flags}{f' with ACK {self.recv_ack_nr}' if flags.ack else ''} "
                      f"with {len(data)} bytes")
        header = _Header(self.local_port, self.remote_port, syn_nr, self.recv_ack_nr if flags.ack else 0,
                         int(flags), self.local_window_size, len(data))
        packet = _TimestampPacket(header, data, time.perf_counter())
        packet.set_checksum()
        self.sent_ack_buffer.append(packet)
        # noinspection PyProtectedMember
        self.binding._send(packet, self.remote_udp_addr)

    def __send_ack(self) -> None:
        logging.debug(self.deb + f"Sending ACK {self.recv_ack_nr}")
        ack_header = _Header(self.local_port, self.remote_port, self.send_syn_nr, self.recv_ack_nr,
                             int(_Flags((False, True, False))), self.local_window_size, 0)

        ack_packet = _Packet(ack_header, b"")
        ack_packet.set_checksum()
        # noinspection PyProtectedMember
        self.binding._send(ack_packet, self.remote_udp_addr)

    def pass_received_msgs(self, packets: List[_AddrPacket]) -> None:
        logging.debug(self.deb + f"Received {len(packets)} messages")
        send_ack = False

        for packet in packets:
            flags = packet.header.flags_obj()
            logging.debug(self.deb + f"Received #{packet.header.syn_nr} {flags}"
                          f"{f' with ACK {packet.header.ack_nr}' if flags.ack else ''} "
                          f"with {len(packet.data)} bytes")

            if self.expect_syn_ack:
                if flags.syn and flags.ack and not flags.fin:
                    if packet.header.ack_nr != self.__next_seq(self.send_ack_nr):
                        logging.warning(self.deb + "Wrong ACK nr in SYN-ACK")
                        continue
                    self.expect_syn_ack = False
                    self.recv_syn_nr = packet.header.syn_nr
                    self.recv_ack_nr = self.__next_seq(packet.header.syn_nr)
                    self.first_recv_syn_nr = self.recv_syn_nr
                    self.remote_window_size = packet.header.window
                    send_ack = True
                else:
                    logging.warning(self.deb + "SYN-ACK expected")
                    continue
            elif flags.syn:
                if self.first_recv_syn_nr is not None and packet.header.syn_nr == self.first_recv_syn_nr:
                    logging.warning(self.deb + "Spurious SYN")
                    continue
                else:
                    logging.warning(self.deb + "Unexpected SYN")
                    # (Would be simultaneous open if self.first_recv_syn_nr is None)
                    continue

            if not self.__seq_between(packet.header.syn_nr, self.recv_ack_nr - self.local_window_size,
                                      self.recv_ack_nr + self.local_window_size - 1):
                logging.info(self.deb + "SYN nr outside of window (spurious retransmission?)")
                continue

            if self.__seq_between(packet.header.syn_nr, self.recv_ack_nr + 1,
                                  self.recv_ack_nr + self.local_window_size - 1):
                self.recv_syn_nr = packet.header.syn_nr

            if flags.ack:
                if self.__seq_between(packet.header.ack_nr, self.send_ack_nr + 1, self.send_syn_nr):
                    # print(f"BEFORE ACK: {', '.join([str(packet.header.syn_nr) for packet in self.sent_ack_buffer])}")
                    # Delete ACKed messages
                    self.sent_ack_buffer = [
                        p for p in self.sent_ack_buffer
                        if not self.__seq_between(p.header.syn_nr, self.send_ack_nr, packet.header.ack_nr - 1)]
                    self.send_ack_nr = packet.header.ack_nr
                    # print(f"AFTER ACK: {', '.join([str(packet.header.syn_nr) for packet in self.sent_ack_buffer])}")
                    if self.send_ack_nr == self.send_syn_nr:
                        self.all_acked_lock.acquire()
                        self.all_acked.notify()
                        self.all_acked_lock.release()

            if flags.fin:
                self.receiver_closed_lock.acquire()
                self.receiver_closed = True
                self.receiver_closed_event.notify()
                self.receiver_closed_lock.release()

            if packet.header.data_length > 0 or flags.fin:
                if packet.header.syn_nr == self.recv_ack_nr:
                    # Move run of received packets to receive buffer
                    self.receive_buffer_lock.acquire()
                    if packet.header.data_length > 0:
                        self.receive_buffer.append(packet.data)
                    self.recv_ack_nr = self.__next_seq(self.recv_ack_nr)

                    index = bisect.bisect_left(self.receive_ack_buffer, packet)
                    while (index < len(self.receive_ack_buffer)
                           and self.receive_ack_buffer[index].header.syn_nr == self.recv_ack_nr):
                        packet = self.receive_ack_buffer.pop(index)
                        if packet.header.data_length > 0:
                            self.receive_buffer.append(packet.data)
                        self.recv_ack_nr = self.__next_seq(self.recv_ack_nr)
                        if len(self.receive_ack_buffer) > 0:
                            index %= len(self.receive_ack_buffer)

                    self.receive_bytes_available.notify()
                    self.receive_buffer_lock.release()

                elif self.__seq_between(packet.header.syn_nr, self.recv_ack_nr + 1,
                                        self.recv_ack_nr + self.local_window_size - 1):
                    # There is a gap, store this packet
                    insert_index = bisect.bisect_left(self.receive_ack_buffer, packet)

                    if (insert_index < len(self.receive_ack_buffer)
                            and self.receive_ack_buffer[insert_index].header.syn_nr == packet.header.syn_nr):
                        if enable_more_asserts:
                            assert self.receive_ack_buffer[insert_index].data == packet.data
                        logging.info(self.deb + "Spurious retransmission of packet in window")
                    else:
                        self.receive_ack_buffer.insert(insert_index, packet)
                else:
                    logging.info(self.deb + "Spurious retransmission of packet before window")

                send_ack = True

        if send_ack:
            # Don't send ACK now if we can piggyback it on the data
            self.send_buffer_lock.acquire()
            data_to_send = len(self.send_buffer) > 0
            self.send_buffer_lock.release()
            if not data_to_send:
                self.__send_ack()

        if debug_window:
            print(self.deb + f"ACK {self.recv_ack_nr}")
            syn = self.recv_ack_nr
            if len(self.receive_ack_buffer) > 0:
                index = bisect.bisect_left(self.receive_ack_buffer, _Packet(_Header(0, 0, syn, 0, 0, 0, 0, 0), b""))
                while syn < self.recv_ack_nr + self.local_window_size:
                    while (self.receive_ack_buffer[index].header.syn_nr != syn
                           < self.recv_ack_nr + self.local_window_size):
                        print(".", end="")
                        syn += 1
                    while (self.receive_ack_buffer[index].header.syn_nr == syn
                           < self.recv_ack_nr + self.local_window_size):
                        print("R", end="")
                        syn += 1
                        index = (index + 1) % len(self.receive_ack_buffer)
                print()
            else:
                print("." * self.local_window_size)

    def poll_sender(self) -> None:
        curtime = time.perf_counter()
        lost = [p for p in self.sent_ack_buffer if curtime - p.timestamp >= self.timeout]
        self.sent_ack_buffer = [p for p in self.sent_ack_buffer if curtime - p.timestamp < self.timeout]

        # print(f"LOST: {', '.join([str(packet.header.syn_nr) for packet in lost])}")
        # print(f"NOT LOST: {', '.join([str(packet.header.syn_nr) for packet in self.sent_ack_buffer])}")
        if len(lost) > 0:
            logging.warning(self.deb + f"{len(lost)} lost messages")
        for p in lost:
            p.timestamp = time.perf_counter()
            self.__send_to_be_acked(p.data, p.header.syn_nr, p.header.flags_obj())
        # print(f"SENT to ACK: {', '.join([str(packet.header.syn_nr) for packet in self.sent_ack_buffer])}\n")

        if self.expect_syn_ack:
            return

        if len(self.send_buffer) > 0:
            logging.debug(self.deb + f"{len(self.send_buffer)} unsent messages")

        if len(self.sent_ack_buffer) < self.remote_window_size:
            self.send_buffer_lock.acquire()
            while len(self.send_buffer) > 0 and len(self.sent_ack_buffer) < self.remote_window_size:
                data = self.send_buffer.pop(0)
                self.__send_to_be_acked(data, self.send_syn_nr)
                self.send_syn_nr = self.__next_seq(self.send_syn_nr)
            if len(self.send_buffer) == 0:
                self.send_buffer_empty.notify()
            self.send_buffer_lock.release()

    def enqueue_data(self, data: bytes) -> None:
        logging.debug(self.deb + f"Enqueuing {len(data)} bytes")
        assert not self.sender_closed
        self.send_buffer_lock.acquire()
        offset = 0
        while offset < len(data):
            self.send_buffer.append(data[offset: min(offset + payload_size, len(data))])
            offset += payload_size
        self.send_buffer_lock.release()

    def __at_least_received(self, count: int) -> bool:
        i = 0
        while i < len(self.receive_buffer) and count > 0:
            count -= len(self.receive_buffer[i])
            i += 1
        logging.debug(self.deb + f"{count} bytes short")
        return count <= 0

    def dequeue_data_array(self, count: int) -> List[bytes]:
        logging.debug(self.deb + f"Dequeuing {count} bytes...")
        self.receive_buffer_lock.acquire()
        self.receive_bytes_available.wait_for(lambda: self.__at_least_received(count))

        data = []
        bytes_received = 0
        while bytes_received + len(self.receive_buffer[0]) < count:
            bytes_received += len(self.receive_buffer[0])
            data.append(self.receive_buffer.pop(0))

        if bytes_received < count:
            bytes_short = count - bytes_received
            data.append(self.receive_buffer[0][:bytes_short])
            self.receive_buffer[0] = self.receive_buffer[0][bytes_short:]

        self.receive_buffer_lock.release()
        logging.debug(self.deb + "Dequeued")
        return data

    def dequeue_data(self, count: int) -> bytes:
        return b"".join(self.dequeue_data_array(count))

    def dequeue_is_available(self, count: int) -> bool:
        self.receive_buffer_lock.acquire()
        available = self.__at_least_received(count)
        self.receive_buffer_lock.release()
        return available

    def dequeue_all_data(self) -> bytes:
        logging.debug(self.deb + "Dequeuing all data...")
        self.receive_buffer_lock.acquire()
        data = b""
        while len(self.receive_buffer) > 0:
            data += self.receive_buffer.pop(0)
        self.receive_buffer_lock.release()
        logging.debug(self.deb + f"Dequeued {len(data)} bytes")
        return data

    def close_sender(self) -> None:
        logging.debug(self.deb + "Close sender")
        if self.sender_closed:
            logging.debug(self.deb + "Sender is already closed")
            return

        logging.debug(self.deb + "Waiting until send_buffer is empty")
        self.send_buffer_lock.acquire()
        self.send_buffer_empty.wait_for(lambda: len(self.send_buffer) == 0)
        self.send_buffer_lock.release()

        logging.debug(self.deb + "Waiting until sent_ack_buffer is empty")
        self.all_acked_lock.acquire()
        self.all_acked.wait_for(lambda: self.send_ack_nr == self.send_syn_nr)
        self.all_acked_lock.release()

        self.__send_to_be_acked(b"", self.send_syn_nr, _Flags((False, True, True)))
        self.send_syn_nr = self.__next_seq(self.send_syn_nr)
        self.sender_closed = True

    def close_receiver(self) -> None:
        """Call after close_sender"""
        logging.debug(self.deb + "Waiting for remote host to close")
        self.receiver_closed_lock.acquire()
        self.receiver_closed_event.wait_for(lambda: self.receiver_closed)
        self.receiver_closed_lock.release()

        time.sleep(self.timeout * 4)


class _Connection:
    def __init__(self, parent: Union["_Server", "Binding"], stream: _Stream):
        self.parent = parent
        self.stream = stream
        self.deb = f"{self.__parent_str()}|{self.stream.local_port}->{self.stream.remote_port}: "
        logging.debug(self.deb + "Setup connection")

    def __parent_str(self) -> str:
        return self.parent.local_udp_addr if type(self.parent) is Binding else self.parent.binding.local_udp_addr

    def _pass_received_msgs(self, packets: List[_AddrPacket]) -> None:
        self.stream.pass_received_msgs(packets)

    def _poll_streams(self) -> None:
        self.stream.poll_sender()

    def send(self, data: bytes) -> None:
        self.stream.enqueue_data(data)

    def receive_array(self, count: int) -> List[bytes]:
        return self.stream.dequeue_data_array(count)

    def receive(self, count: int) -> bytes:
        return self.stream.dequeue_data(count)

    def receive_is_available(self, count: int) -> bool:
        return self.stream.dequeue_is_available(count)

    def receive_all(self) -> bytes:
        return self.stream.dequeue_all_data()

    def close_sender(self) -> None:
        logging.debug(self.deb + "Close sender")
        self.stream.close_sender()

    def close(self) -> None:
        logging.debug(self.deb + "Closing connection...")
        self.stream.close_sender()
        self.stream.close_receiver()
        if type(self.parent) is _Server:
            # noinspection PyProtectedMember
            self.parent._remove_stream(self.stream.remote_port)
        else:
            # noinspection PyProtectedMember
            self.parent._remove_socket(self.stream.local_port)
        logging.debug(self.deb + "Closed")


class _Server:
    def __init__(self, binding: "Binding", local_port: int):
        self.binding = binding
        self.local_port = local_port
        self.streams: Dict[int, _Stream] = {}  # Remote port -> _Stream
        self.streams_lock = threading.Lock()

        self.backlog = 0
        self.backlog_connections: List[_Connection] = []
        self.backlog_lock = threading.Lock()
        self.backlog_available = threading.Condition(self.backlog_lock)

        self.deb = f"{self.binding.local_udp_addr}|{self.local_port}: "
        logging.debug(self.deb + "Setup server")

    def _pass_received_msgs(self, packets: List[_AddrPacket]) -> None:
        logging.debug(self.deb + f"Received {len(packets)} messages")

        packet_batches: Dict[int, List[_AddrPacket]] = {}
        orphan_packets: List[_AddrPacket] = []

        self.streams_lock.acquire()

        for packet in packets:
            if packet.header.src_port in self.streams:
                packet_batches.setdefault(packet.header.src_port, []).append(packet)
            else:
                orphan_packets.append(packet)

        for remote_port, packets in packet_batches.items():
            self.streams[remote_port].pass_received_msgs(packets)

        for packet in orphan_packets:
            flags = packet.header.flags_obj()
            if (flags.syn and not flags.ack and not flags.fin
                    and packet.header.data_length == 0):
                self.backlog_lock.acquire()
                if self.backlog > 0:
                    self.streams[packet.header.src_port] = stream = _Stream(
                        self.binding, self.local_port,
                        packet.header.src_port, packet.remote_udp_addr,
                        self.binding.window_size, self.binding.timeout_sec,
                        _RemoteInit(packet.header.syn_nr, packet.header.window))
                    self.backlog_connections.append(_Connection(self, stream))
                    self.backlog -= 1
                    self.backlog_available.notify()
                    self.backlog_lock.release()
                else:
                    self.backlog_lock.release()
                    logging.warning(self.deb + "Backlog full")
            else:
                logging.warning(self.deb + "Unknown connection")

        self.streams_lock.release()

    def _poll_streams(self) -> None:
        self.streams_lock.acquire()
        for stream in self.streams.values():
            stream.poll_sender()
        self.streams_lock.release()

    def _remove_stream(self, remote_port: int) -> None:
        logging.debug(self.deb + "Remove stream")
        self.streams_lock.acquire()
        del self.streams[remote_port]
        self.streams_lock.release()

    def start_listen(self, backlog: int) -> None:
        logging.debug(self.deb + "Start listen")
        self.backlog_lock.acquire()
        self.backlog = backlog
        self.backlog_lock.release()

    def accept(self) -> _Connection:
        logging.debug(self.deb + "Accepting...")
        self.backlog_lock.acquire()
        self.backlog_available.wait_for(lambda: len(self.backlog_connections) > 0)
        connection = self.backlog_connections.pop(0)
        self.backlog_lock.release()
        logging.debug(self.deb + "Accepted")
        return connection

    def close(self) -> None:
        logging.debug(self.deb + "Closing...")
        while len(self.streams) > 0:
            self.streams[0].close_sender()
            self.streams[0].close_receiver()
            self.streams.pop(0)
        # noinspection PyProtectedMember
        self.binding._remove_socket(self.local_port)
        logging.debug(self.deb + "Closed")


class Binding:
    def __init__(self, protocol: int, local_udp_addr: Optional[Any],
                 window_size: int, timeout_ms: int, poll_time_ms: Optional[int] = None):
        self.local_udp_addr = local_udp_addr
        self.window_size = window_size
        self.timeout_sec = timeout_ms / 1000
        self.poll_time_ms = poll_time_ms or min(25., timeout_ms / 4)

        self.sock = socket.socket(protocol, socket.SOCK_DGRAM)
        if local_udp_addr is not None:
            self.sock.bind(local_udp_addr)

        self.sockets: Dict[int, Union[_Server, _Connection]] = {}  # Local port -> _Server / _Connection
        self.stop = False
        self.stop_lock = threading.Lock()
        self.sockets_lock = threading.Lock()

        self.deb = f"{self.local_udp_addr}: "
        self.background_thread = threading.Thread(None, self.__background)
        self.background_thread.start()

        logging.debug(self.deb + "Set up binding")

    def __background(self) -> None:
        while True:
            self.stop_lock.acquire()
            if self.stop:
                self.stop_lock.release()
                break
            self.stop_lock.release()

            self.sock.settimeout(self.poll_time_ms / 1000)
            no_more_data = False
            try:
                data, addr = self.sock.recvfrom(header_size + payload_size)
                while len(data) < header_size + payload_size:
                    self.sock.settimeout(None)
                    data += self.sock.recv(header_size + payload_size - len(data))
            except (socket.timeout, BlockingIOError):
                no_more_data = True

            packet_batches: Dict[int, List[_AddrPacket]] = {}

            self.sockets_lock.acquire()
            while not no_more_data:
                # noinspection PyUnboundLocalVariable
                logging.debug(self.deb + f"Packet from {addr}")
                # noinspection PyUnboundLocalVariable
                packet = _AddrPacket(_Header.unpack(data[:header_size]),
                                     data[header_size:], addr)
                if packet.header.data_length > payload_size:
                    logging.warning(self.deb + "data_length too large")
                packet.data = packet.data[:packet.header.data_length]

                if not packet.verify_checksum():
                    logging.warning(self.deb + "Invalid checksum")
                else:
                    if packet.header.dest_port in self.sockets:
                        packet_batches.setdefault(packet.header.dest_port, []).append(packet)
                    else:
                        logging.warning(self.deb + "Unknown server")

                self.sock.settimeout(0)
                try:
                    data, addr = self.sock.recvfrom(header_size + payload_size)
                    while len(data) < header_size + payload_size:
                        self.sock.settimeout(None)
                        data += self.sock.recv(header_size + payload_size - len(data))
                except (socket.timeout, BlockingIOError):
                    no_more_data = True

            self.sock.settimeout(None)
            for local_port, packets in packet_batches.items():
                # noinspection PyProtectedMember
                self.sockets[local_port]._pass_received_msgs(packets)

            for server in self.sockets.values():
                # noinspection PyProtectedMember
                server._poll_streams()

            self.sockets_lock.release()

    def _send(self, packet: _Packet, remote_udp_addr: Any) -> None:
        logging.debug(self.deb + f"Send to {remote_udp_addr}")
        data = bytes(packet.header) + packet.data + bytes(payload_size - len(packet.data))
        while len(data) > 0:
            data = data[self.sock.sendto(data, remote_udp_addr):]

    def _remove_socket(self, local_port: int) -> None:
        logging.debug(self.deb + f"Remove socket {local_port}")
        self.sockets_lock.acquire()
        del self.sockets[local_port]
        self.sockets_lock.release()

    def bind_server(self, local_btcp_port: int) -> _Server:
        logging.debug(self.deb + f"Bind server to btcp {local_btcp_port}")
        self.sockets_lock.acquire()
        server = self.sockets[local_btcp_port] = _Server(self, local_btcp_port)
        self.sockets_lock.release()
        return server

    def connect_client(self, local_btcp_port: int, remote_btcp_port: int, remote_udp_addr: Any) -> _Connection:
        logging.debug(self.deb + f"Connect to btcp {local_btcp_port} -> {remote_btcp_port} {remote_udp_addr}")
        self.sockets_lock.acquire()
        connection = _Connection(self, _Stream(self, local_btcp_port, remote_btcp_port, remote_udp_addr,
                                               self.window_size, self.timeout_sec))
        self.sockets[local_btcp_port] = connection
        self.sockets_lock.release()
        return connection

    def close(self) -> None:
        logging.debug(self.deb + "Closing...")

        while len(self.sockets) > 0:
            key = None
            for key in self.sockets.keys():
                break
            self.sockets[key].close()

        self.stop_lock.acquire()
        self.stop = True
        self.stop_lock.release()

        self.background_thread.join()
        self.sock.close()
        logging.debug(self.deb + "Closed")
