#!/bin/python3
# s1005864 Lars Kuipers, s1011387 Steven Wallis de Vries

import binascii
import sys
import threading
import unittest
import os
import random

timeout = 100
winsize = 100
intf = "lo"
netem_add = "sudo tc qdisc add dev {} root netem".format(intf)
netem_change = "sudo tc qdisc change dev {} root netem {}".format(intf, "{}")
netem_del = "sudo tc qdisc del dev {} root netem".format(intf)

infile = "infile"
outfile = "outfile"

server_thread: threading.Thread


def run_command_with_output(command, input=None, cwd=None, shell=True):
    """run command and retrieve output"""
    import subprocess
    try:
        process = subprocess.Popen(command, cwd=cwd, shell=shell, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
    except Exception as inst:
        print("problem running command : \n   ", str(command))

    [stdoutdata, stderrdata] = process.communicate(
        input)  # no pipes set for stdin/stdout/stdout streams so does effectively only just wait for process ends  (same as process.wait()

    if process.returncode:
        print(stderrdata)
        print("problem running command : \n   ", str(command), " ", process.returncode)

    return stdoutdata


def run_command(command, cwd=None, shell=True):
    """run command with no output piping"""
    import subprocess
    process = None
    try:
        process = subprocess.Popen(command, shell=shell, cwd=cwd)
        print(str(process))
    except Exception as inst:
        print("1. problem running command : \n   ", str(command), "\n problem : ", str(inst))

    process.communicate()  # wait for the process to end

    if process.returncode:
        print("2. problem running command : \n   ", str(command), " ", process.returncode)


def transfer_file() -> None:
    # launch localhost client connecting to server
    # client sends content to server
    # server receives content from client
    run_command_with_output(run_client)

    global server_thread
    server_thread.join()

    # content received by server matches the content sent by client
    file = open(infile, "r+b")
    crc_in = binascii.crc32(file.read())
    file.close()

    file = open(outfile, "r+b")
    crc_out = binascii.crc32(file.read())
    file.close()

    assert crc_out == crc_in


class TestbTCPFramework(unittest.TestCase):
    """Test cases for bTCP"""

    def setUp(self):
        """Prepare for testing"""
        # default netem rule (does nothing)
        run_command(netem_add)

        try:
            os.remove(outfile)
        except OSError:
            pass

        # launch localhost server
        global server_thread
        server_thread = threading.Thread(target=lambda: run_command_with_output(run_server))
        server_thread.start()

    def tearDown(self):
        """Clean up after testing"""
        # clean the environment
        run_command(netem_del)

        # close server
        # Already done

        try:
            os.remove(outfile)
        except OSError:
            pass

    def test_ideal_network(self):
        print("reliability over an ideal framework")
        # setup environment (nothing to set)

        transfer_file()

    def test_flipping_network(self):
        print("reliability over network with bit flips")
        """(which sometimes results in lower layer packet loss)"""
        # setup environment
        run_command(netem_change.format("corrupt 1%"))

        transfer_file()

    def test_duplicates_network(self):
        print("reliability over network with duplicate packets")
        # setup environment
        run_command(netem_change.format("duplicate 10%"))

        transfer_file()

    def test_lossy_network(self):
        print("reliability over network with packet loss")
        # setup environment
        run_command(netem_change.format("loss 10% 25%"))

        transfer_file()

    def test_reordering_network(self):
        print("reliability over network with packet reordering")
        # setup environment
        run_command(netem_change.format("delay 20ms reorder 25% 50%"))

        transfer_file()

    def test_delayed_network(self):
        print("reliability over network with delay relative to the timeout value")
        # setup environment
        run_command(netem_change.format("delay " + str(timeout) + "ms 20ms"))

        transfer_file()

    def test_allbad_network(self):
        print("reliability over network with all of the above problems")

        # setup environment
        run_command(netem_change.format("corrupt 1% duplicate 10% loss 10% 25% delay 20ms reorder 25% 50%"))

        transfer_file()


if __name__ == "__main__":
    # Parse command line arguments
    import argparse

    parser = argparse.ArgumentParser(description="bTCP tests")
    parser.add_argument("-w", "--window", help="Define bTCP window size used", type=int, default=winsize)
    parser.add_argument("-t", "--timeout", help="Define the timeout value used (ms)", type=int, default=timeout)
    args, extra = parser.parse_known_args()
    timeout = args.timeout
    winsize = args.window

    if not os.path.isfile(infile):
        file = open(infile, "w+b")
        data = []
        bytes_left = 5_000_000
        while bytes_left > 0:
            data.append(random.getrandbits(8))
            bytes_left -= 1
        file.write(bytes(data))
        file.close()

    run_client = f"./bTCP_client.py -t {timeout} -w {winsize} -i {infile}"
    run_server = f"./bTCP_server.py -t {timeout} -w {winsize} -o {outfile}"

    # Pass the extra arguments to unittest
    sys.argv[1:] = extra

    # Start test suite
    unittest.main()
